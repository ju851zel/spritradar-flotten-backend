package com.formigas.spritradar.provider;

interface AuthProvider {
    fun isAuthorized(token: String): Boolean
}
