package com.formigas.spritradar.provider

import com.formigas.spritradar.controller.GroupNameId
import com.formigas.spritradar.model.CreateGroupModel
import com.formigas.spritradar.model.RefuelEntry
import com.formigas.spritradar.model.RouteEntry
import com.formigas.spritradar.model.Vehicle

interface DatabaseProvider {
    fun createGroup(token: String, enterprise: CreateGroupModel): String
    fun joinGroup(groupID: String, token: String)
    fun createVehicle(token: String, groupID: String, vehicle: Vehicle)
    fun isAuthorized(token: String): Boolean
    fun createRoute(token: String, groupID: String, route: RouteEntry, vehicleID: String)
    fun createRefuelEntry(token: String, groupID: String, refuelEntry: RefuelEntry, vehicleID: String)
    fun getAllGroupVehicles(token: String, groupID: String): List<Vehicle>
    fun getAllVehicleRefuelEntries(token: String, groupID: String, vehicleID: String): List<RefuelEntry>
    fun getAllRouteEntries(token: String, groupID: String, vehicleID: String): List<RouteEntry>
    fun getAllGroupsImIn(token: String, groupID: String): List<GroupNameId>
    fun leaveGroup(groupID: String, token: String)
    fun deleteVehicle(groupID: String, token: String, vehicleID: String)
    fun deleteRefuelEntry(groupID: String, token: String, vehicleID: String, refuelID: String)
    fun deleteRouteEntry(groupID: String, token: String, vehicleID: String, routeID: String)
    fun isEnterprise(groupID: String): String?

}