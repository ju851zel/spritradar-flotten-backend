package com.formigas.spritradar.provider

import com.formigas.spritradar.controller.GroupNameId
import com.formigas.spritradar.model.*
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.firestore.FieldValue
import com.google.cloud.firestore.Firestore
import com.google.cloud.firestore.FirestoreOptions
import com.google.cloud.storage.BlobId
import com.google.cloud.storage.BlobInfo
import com.google.cloud.storage.Storage
import com.google.cloud.storage.StorageOptions
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.joda.time.DateTime
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import java.net.URI
import java.net.URLEncoder
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.util.*


const val CLOUD_CREDENTIALS_ENV_VAR = "CLOUD_CREDENTIALS"
const val CLOUD_BUCKET_ENV_VAR = "CLOUD_BUCKET"

private const val GROUPS_COLLECTION: String = "groups"
private const val VEHICLE_COLLECTION: String = "vehicles"
private const val ROUTE_COLLECTION: String = "routes"
private const val REFUELENTRY_COLLECTION: String = "refuels"

class ProviderFirebase : DatabaseProvider, AuthProvider {
    private val firestore: Firestore
    private val auth: FirebaseAuth
    private val storage: Storage
    val fileName = "enterprise_customers.json"


    private val CLOUD_BUCKET: String = System.getenv(CLOUD_BUCKET_ENV_VAR)
        ?: throw Exception("no cloud bucket env var specified for CLOUD_BUCKET")
    private val CREDENTIALS: String = System.getenv(CLOUD_CREDENTIALS_ENV_VAR)
        ?: throw Exception("no cloud bucket env var specified for CLOUD_CREDENTIALS")

    init {
        ServiceAccountCredentials.fromStream(CREDENTIALS.byteInputStream(Charsets.UTF_8)).also { credentials ->
            firestore = FirestoreOptions.newBuilder().setCredentials(credentials).build().service
            val options = FirebaseOptions.builder().setCredentials(credentials).build()
            val app = FirebaseApp.initializeApp(options)
            auth = FirebaseAuth.getInstance(app)
            storage = StorageOptions.newBuilder().setCredentials(credentials).build().service
        }
    }


    override fun isAuthorized(token: String): Boolean {
        return try {
            auth.verifyIdToken(token)
            true
        } catch (e: FirebaseAuthException) {
            print(e)
            false
        }
    }

    override fun createGroup(token: String, enterprise: CreateGroupModel): String =
        getUserIdFromToken(token)!!.also { userID ->
            Group(
                groupID = userID,
                memberIDs = listOf(getUserIdFromToken(token)!!),
                ownerID = userID,
                creationDateIso = DateTime.now().toString(),
                enterprise = enterprise.isEnterprise,
                name = enterprise.name,
            ).also {
                val result = firestore.collection(GROUPS_COLLECTION).document(it.groupID).set(it).get()
                print(result.toString())
                if (enterprise.isEnterprise) {
                    createKubernetesDeployment(it.groupID)
                }
            }.groupID
        }

    private fun createKubernetesDeployment(groupID: String) {
        val bucket = storage.get(CLOUD_BUCKET)
        val doc = bucket?.get(fileName)
        val type = object : TypeToken<List<String>>() {}.type
        val stringContent: String? = doc?.getContent()?.toString(Charsets.UTF_8)
        val enterpriseCustomers: List<String> = Gson().fromJson(stringContent, type)
        val lsit = enterpriseCustomers.toMutableList()
        lsit.add(groupID)
        BlobInfo.newBuilder(BlobId.of(CLOUD_BUCKET, fileName))
            .setContentType("text/json")
            .build().also { blobInfo ->
                storage.create(
                    blobInfo,
                    Gson().toJson(lsit).toByteArray(Charsets.UTF_8)
                )
            }
        Thread.sleep(1000)


        val values = mapOf("token" to "4e6283b5de268c90e73b56f3af0ade", "ref" to "main")

        val client = HttpClient.newBuilder().build();
        val request = HttpRequest.newBuilder()
            .uri(URI.create("https://gitlab.com/api/v4/projects/32209750/trigger/pipeline"))
            .POST(formData(values))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .build()
        val response = client.send(request, HttpResponse.BodyHandlers.ofString());

        println(response.body())

    }

    fun String.utf8(): String = URLEncoder.encode(this, "UTF-8")
    fun formData(data: Map<String, String>): HttpRequest.BodyPublisher? {
        val res = data.map { (k, v) -> "${(k.utf8())}=${v.utf8()}" }
            .joinToString("&")
        return HttpRequest.BodyPublishers.ofString(res)
    }

    override fun joinGroup(groupID: String, token: String) {
        UUID.randomUUID().toString().apply {
            throw404IfGroupNotFound(groupID)
            val result =
                firestore.collection(GROUPS_COLLECTION)
                    .document(groupID)
                    .update("memberIDs", FieldValue.arrayUnion(getUserIdFromToken(token)))
                    .get()
            print(result.toString())
        }
    }

    override fun createVehicle(token: String, groupID: String, vehicle: Vehicle) {
        throw404IfGroupNotFound(groupID)
        if (firestore.collection(GROUPS_COLLECTION).document(groupID).collection(VEHICLE_COLLECTION)
                .document(vehicle.id).get().get().exists()
        ) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Vehicle already exists")
        }
        val result = firestore.collection(GROUPS_COLLECTION).document(groupID).collection(VEHICLE_COLLECTION)
            .document(vehicle.id).set(vehicle).get()
        print(result.toString())
    }

    override fun createRoute(token: String, groupID: String, route: RouteEntry, vehicleID: String) {
        if (firestore.collection(GROUPS_COLLECTION).document(groupID).get().get().exists().not()) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Group not found ")
        }
        if (firestore.collection(GROUPS_COLLECTION).document(groupID)
                .collection(VEHICLE_COLLECTION)
                .document(vehicleID).get().get().exists().not()
        ) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Vehicle in group not found ")
        }
        if (firestore.collection(GROUPS_COLLECTION).document(groupID).collection(VEHICLE_COLLECTION)
                .document(vehicleID).collection(ROUTE_COLLECTION)
                .document(route.id).get().get().exists()
        ) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Route entry exists")
        }
        val result = firestore.collection(GROUPS_COLLECTION).document(groupID).collection(VEHICLE_COLLECTION)
            .document(vehicleID).collection(ROUTE_COLLECTION)
            .document(route.id).set(route).get()
        print(result.toString())

    }

    override fun createRefuelEntry(token: String, groupID: String, refuelEntry: RefuelEntry, vehicleID: String) {
        if (firestore.collection(GROUPS_COLLECTION).document(groupID).get().get().exists().not()) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Group not found ")
        }
        if (firestore.collection(GROUPS_COLLECTION).document(groupID)
                .collection(VEHICLE_COLLECTION)
                .document(vehicleID).get().get().exists().not()
        ) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Vehicle in group not found ")
        }
        if (firestore.collection(GROUPS_COLLECTION).document(groupID).collection(VEHICLE_COLLECTION)
                .document(vehicleID).collection(REFUELENTRY_COLLECTION)
                .document(refuelEntry.id).get().get().exists()
        ) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Refuel entry exists")
        }
        val result = firestore.collection(GROUPS_COLLECTION).document(groupID).collection(VEHICLE_COLLECTION)
            .document(vehicleID).collection(REFUELENTRY_COLLECTION)
            .document(refuelEntry.id).set(refuelEntry).get()
        print(result.toString())
    }

    override fun getAllGroupVehicles(token: String, groupID: String): List<Vehicle> {
        val allVehicles: List<MutableMap<String, Any>> =
            firestore.collection(GROUPS_COLLECTION).document(groupID).collection(VEHICLE_COLLECTION).get()
                .get().documents.map { it.data }
        return allVehicles.toList().map { Gson().fromJson(Gson().toJsonTree(it), Vehicle::class.java) }
    }

    override fun getAllVehicleRefuelEntries(token: String, groupID: String, vehicleID: String): List<RefuelEntry> {
        val allRefuelEntries: List<MutableMap<String, Any>> =
            firestore.collection(GROUPS_COLLECTION).document(groupID).collection(
                VEHICLE_COLLECTION
            ).document(vehicleID).collection(REFUELENTRY_COLLECTION).get().get().documents.map { it.data }
        return allRefuelEntries.toList().map { Gson().fromJson(Gson().toJsonTree(it), RefuelEntry::class.java) }
    }

    private fun throw404IfGroupNotFound(groupID: String) {
        if (firestore.collection(GROUPS_COLLECTION).document(groupID).get().get().exists().not()) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Group not found")
        }
    }

    override fun getAllRouteEntries(token: String, groupID: String, vehicleID: String): List<RouteEntry> {
        val allRouteEntries: List<MutableMap<String, Any>> =
            firestore.collection(GROUPS_COLLECTION).document(groupID).collection(
                VEHICLE_COLLECTION
            ).document(vehicleID).collection(ROUTE_COLLECTION).get().get().documents.map { it.data }
        return allRouteEntries.toList().map { Gson().fromJson(Gson().toJsonTree(it), RouteEntry::class.java) }
    }

    override fun getAllGroupsImIn(token: String, groupID: String): List<GroupNameId> {
        val groups = firestore.collection(GROUPS_COLLECTION).listDocuments().map { Pair(it.id, it.get().get().data) }
        val groupsStrings = groups.filter {
            (it.second?.get("memberIDs") as List<*>).contains(groupID)
        }.map { it.first }

        return firestore.collection(GROUPS_COLLECTION).listDocuments().filter { groupsStrings.contains(it.id) }
            .map { GroupNameId((it.get().get().data?.get("name") as String) , it.id) }
    }

    override fun leaveGroup(groupID: String, token: String) {
        throw404IfGroupNotFound(groupID)
        val result = firestore.collection(GROUPS_COLLECTION)
            .document(groupID)
            .update("memberIDs", FieldValue.arrayRemove(getUserIdFromToken(token)))
            .get()
        print(result.toString())
    }

    override fun deleteVehicle(groupID: String, token: String, vehicleID: String) {
        throw404IfGroupNotFound(groupID)
        if (firestore.collection(GROUPS_COLLECTION).document(groupID)
                .collection(VEHICLE_COLLECTION)
                .document(vehicleID).get().get().exists().not()
        ) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Vehicle in group not found ")
        }
        val result = firestore.collection(GROUPS_COLLECTION)
            .document(groupID)
            .collection(VEHICLE_COLLECTION)
            .document(vehicleID)
            .delete().get()
        print(result.toString())
    }

    override fun deleteRefuelEntry(groupID: String, token: String, vehicleID: String, refuelID: String) {
        throw404IfGroupNotFound(groupID)
        if (firestore.collection(GROUPS_COLLECTION).document(groupID)
                .collection(VEHICLE_COLLECTION)
                .document(vehicleID).get().get().exists().not()
        ) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Vehicle in group not found ")
        }
        val result = firestore.collection(GROUPS_COLLECTION)
            .document(groupID)
            .collection(VEHICLE_COLLECTION)
            .document(vehicleID)
            .collection(REFUELENTRY_COLLECTION)
            .document(refuelID)
            .delete().get()
        print(result.toString())
    }

    override fun deleteRouteEntry(groupID: String, token: String, vehicleID: String, routeID: String) {
        throw404IfGroupNotFound(groupID)
        if (firestore.collection(GROUPS_COLLECTION).document(groupID)
                .collection(VEHICLE_COLLECTION)
                .document(vehicleID).get().get().exists().not()
        ) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Vehicle in group not found ")
        }
        val result = firestore.collection(GROUPS_COLLECTION)
            .document(groupID)
            .collection(VEHICLE_COLLECTION)
            .document(vehicleID)
            .collection(ROUTE_COLLECTION)
            .document(routeID)
            .delete().get()
        print(result.toString())
    }

    override fun isEnterprise(groupID: String): String? {
        if (firestore.collection(GROUPS_COLLECTION).document(groupID).get().get().exists().not()) {
            throw ResponseStatusException(HttpStatus.CONFLICT, "Vehicle in group not found ")
        }

        val imIn = getAllGroupsImIn("", groupID).map { it.id }
        for (group in imIn) {
            val doc: MutableMap<String, Any> = firestore.collection(GROUPS_COLLECTION).document(group).get().get().data
                ?: return null
            if (Gson().fromJson(Gson().toJsonTree(doc), Group::class.java).enterprise) {
                return group
            }
        }
        return null;
    }

    private fun getUserIdFromToken(token: String): String? = auth.verifyIdToken(token).uid

}

