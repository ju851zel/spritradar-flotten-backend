package com.formigas.spritradar.provider

import com.formigas.spritradar.controller.GroupNameId
import com.formigas.spritradar.model.*


class ProviderInMemory : DatabaseProvider {
    private var petrolStations: List<PetrolStation> = listOf()

    override fun createGroup(userID: String, enterprise: CreateGroupModel): String {
        TODO("Not yet implemented")
    }

    override fun joinGroup(groupID: String, token: String) {
        TODO("Not yet implemented")
    }

    override fun createVehicle(token: String, groupID: String, vehicle: Vehicle) {
        TODO("Not yet implemented")
    }

    override fun isAuthorized(userID: String): Boolean {
        TODO("Not yet implemented")
    }

    override fun createRoute(token: String, groupID: String, route: RouteEntry, vehicleID: String) {
        TODO("Not yet implemented")
    }

    override fun createRefuelEntry(token: String, groupID: String, station: RefuelEntry, vehicleID: String) {
        TODO("Not yet implemented")
    }

    override fun getAllGroupVehicles(token: String, groupID: String): List<Vehicle> {
        TODO("Not yet implemented")
    }

    override fun getAllVehicleRefuelEntries(token: String, groupID: String, vehicleID: String): List<RefuelEntry> {
        TODO("Not yet implemented")
    }

    override fun getAllRouteEntries(token: String, groupID: String, vehicleID: String): List<RouteEntry> {
        TODO("Not yet implemented")
    }

    override fun getAllGroupsImIn(token: String, groupID: String): List<GroupNameId> {
        TODO("Not yet implemented")
    }

    override fun leaveGroup(groupID: String, token: String) {
        TODO("Not yet implemented")
    }

    override fun deleteVehicle(groupID: String, token: String, vehicleID: String) {
        TODO("Not yet implemented")
    }

    override fun deleteRefuelEntry(groupID: String, token: String, vehicleID: String, refuelID: String) {
        TODO("Not yet implemented")
    }

    override fun deleteRouteEntry(groupID: String, token: String, vehicleID: String, routeID: String) {
        TODO("Not yet implemented")
    }

    override fun isEnterprise(groupID: String): String? {
        TODO("Not yet implemented")
    }

}
