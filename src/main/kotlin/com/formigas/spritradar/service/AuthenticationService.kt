package com.formigas.spritradar.service

import com.formigas.spritradar.provider.AuthProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AuthenticationService {
    @Autowired
    lateinit var provider: AuthProvider

    fun isAuthorized(token: String): Boolean = provider.isAuthorized(token)
}

