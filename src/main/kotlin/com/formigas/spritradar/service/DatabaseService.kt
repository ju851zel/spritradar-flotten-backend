package com.formigas.spritradar.service

import com.formigas.spritradar.controller.GroupNameId
import com.formigas.spritradar.model.CreateGroupModel
import com.formigas.spritradar.model.RefuelEntry
import com.formigas.spritradar.model.RouteEntry
import com.formigas.spritradar.model.Vehicle
import com.formigas.spritradar.provider.DatabaseProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DatabaseService {
    @Autowired
    lateinit var provider: DatabaseProvider

    fun createGroup(token: String, enterprise: CreateGroupModel): String = provider.createGroup(token, enterprise)
    fun joinGroup(groupID: String, token: String) =
        provider.joinGroup(groupID = groupID, token = token)

    fun createVehicle(token: String, groupID: String, vehicle: Vehicle) =
        provider.createVehicle(token, groupID, vehicle)

    fun createRoute(token: String, groupID: String, route: RouteEntry, vehicleID: String) =
        provider.createRoute(token, groupID, route, vehicleID)

    fun createRefuelEntry(token: String, groupID: String, refuelEntry: RefuelEntry, vehicleID: String) =
        provider.createRefuelEntry(token, groupID, refuelEntry, vehicleID)

    fun getAllGroupVehicles(token: String, groupID: String): List<Vehicle> =
        provider.getAllGroupVehicles(token = token, groupID = groupID)

    fun getAllVehicleRefuelEntries(token: String, groupID: String, vehicleID: String): List<RefuelEntry> =
        provider.getAllVehicleRefuelEntries(token = token, groupID = groupID, vehicleID = vehicleID)

    fun getAllRouteEntries(token: String, groupID: String, vehicleID: String): List<RouteEntry> =
        provider.getAllRouteEntries(token = token, groupID = groupID, vehicleID = vehicleID)

    fun getAllGroupsImIn(groupID: String, token: String): List<GroupNameId> =
        provider.getAllGroupsImIn(token = token, groupID = groupID)

    fun leaveGroup(groupID: String, token: String) =
        provider.leaveGroup(groupID = groupID, token = token)

    fun deleteVehicle(token: String, groupID: String, vehicleID: String) =
        provider.deleteVehicle(groupID = groupID, token = token, vehicleID = vehicleID)

    fun deleteRefuelEntry(token: String, groupID: String, refuelID: String, vehicleID: String) =
        provider.deleteRefuelEntry(groupID = groupID, token = token, vehicleID = vehicleID, refuelID = refuelID)

    fun deleteRouteEntry(token: String, groupID: String, routeID: String, vehicleID: String) =
        provider.deleteRouteEntry(groupID = groupID, token = token, vehicleID = vehicleID, routeID = routeID)

    fun isEnterprise(groupID: String): String? =
        provider.isEnterprise(groupID = groupID)

}

