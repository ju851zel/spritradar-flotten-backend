package com.formigas.spritradar.model

data class RouteEntry(
    val id: String,
    val departure: String,
    val destination: String,
    val name: String,
    val distanceMeters: String,
    val consumptionMilliliter: Int,
    val totalCostsCent: Int,
    val creationDateIsoString: String,
)