package com.formigas.spritradar.model

data class Group(
    val name: String,
    val groupID: String,
    val memberIDs: List<String>,
    val ownerID: String,
    val creationDateIso: String,
    val enterprise: Boolean,
)

data class CreateGroupModel(val name: String, val isEnterprise: Boolean)
