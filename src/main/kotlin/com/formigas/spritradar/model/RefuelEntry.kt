package com.formigas.spritradar.model

data class RefuelEntry(
    val id: String,
    val position: String,
    val mileageMeters: String,
    val consumptionMilliliter: Int,
    val pricePerLiterCents: Int,
    val totalCostsCent: Int,
    val creationDateIsoString: String,
)
