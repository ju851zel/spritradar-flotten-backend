package com.formigas.spritradar.model

data class Vehicle(
    val id: String,
    val name: String,
    val brand: String,
)
