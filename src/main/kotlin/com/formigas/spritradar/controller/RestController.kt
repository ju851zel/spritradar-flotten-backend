package com.formigas.spritradar.controller

import com.formigas.spritradar.model.CreateGroupModel
import com.formigas.spritradar.model.RefuelEntry
import com.formigas.spritradar.model.RouteEntry
import com.formigas.spritradar.model.Vehicle
import com.formigas.spritradar.service.AuthenticationService
import com.formigas.spritradar.service.DatabaseService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

data class GroupNameId(val name:String, val id: String)

@RestController
@CrossOrigin(origins = ["*"])
class RestController(private val dbService: DatabaseService, private val authService: AuthenticationService) {

    @GetMapping("/alive")
    fun addVehicle(): ResponseEntity<String> =
        ResponseEntity("I'm alive", HttpStatus.OK)

    @PostMapping("/groups/{groupID}/vehicles")
    fun addVehicle(
        @PathVariable("groupID") groupID: String,
        @RequestHeader("Authorization") token: String,
        @RequestBody vehicle: Vehicle
    ) {
        throwUnauthorizedIfSo(token).also {
            return dbService.createVehicle(removeBearerFromToken(token), groupID, vehicle)
        }
    }

    @DeleteMapping("/groups/{groupID}/vehicles/{vehicleID}")
    fun addVehicle(
        @PathVariable("groupID") groupID: String,
        @RequestHeader("Authorization") token: String,
        @PathVariable("vehicleID") vehicleID: String,
    ) {
        throwUnauthorizedIfSo(token).also {
            return dbService.deleteVehicle(removeBearerFromToken(token), groupID, vehicleID)
        }
    }
    // TODO: post mapping to get all entries of all vehicles


    @PostMapping("/groups/{groupID}/vehicles/{vehicleID}/routes")
    fun addRoute(
        @PathVariable("groupID") groupID: String,// TODO: vehicle path
        @PathVariable("vehicleID") vehicleID: String,
        @RequestHeader("Authorization") token: String,
        @RequestBody route: RouteEntry
    ) {
        throwUnauthorizedIfSo(token).also {
            return dbService.createRoute(removeBearerFromToken(token), groupID, route, vehicleID)
        }
    }

    @DeleteMapping("/groups/{groupID}/vehicles/{vehicleID}/routes/{routeID}")
    fun deleteRouteEntry(
        @PathVariable("groupID") groupID: String,// TODO: Fuel path??
        @PathVariable("vehicleID") vehicleID: String,
        @PathVariable("routeID") routeID: String,
        @RequestHeader("Authorization") token: String,
    ) {
        throwUnauthorizedIfSo(token).also {
            return dbService.deleteRouteEntry(removeBearerFromToken(token), groupID, routeID, vehicleID)
        }
    }

    @PostMapping("/groups/{groupID}/vehicles/{vehicleID}/refuels")
    fun addRefuelEntry(
        @PathVariable("groupID") groupID: String,// TODO: Fuel path??
        @PathVariable("vehicleID") vehicleID: String,
        @RequestHeader("Authorization") token: String,
        @RequestBody refuelEntry: RefuelEntry
    ) {
        throwUnauthorizedIfSo(token).also {
            return dbService.createRefuelEntry(removeBearerFromToken(token), groupID, refuelEntry, vehicleID)
        }
    }

    @DeleteMapping("/groups/{groupID}/vehicles/{vehicleID}/refuels/{refuelID}")
    fun deleteRefuelEntry(
        @PathVariable("groupID") groupID: String,// TODO: Fuel path??
        @PathVariable("vehicleID") vehicleID: String,
        @PathVariable("refuelID") refuelID: String,
        @RequestHeader("Authorization") token: String,
    ) {
        throwUnauthorizedIfSo(token).also {
            return dbService.deleteRefuelEntry(removeBearerFromToken(token), groupID, refuelID, vehicleID)
        }
    }


    @PostMapping("/groups")
    fun createGroup(
        @RequestHeader("Authorization") token: String,
        @RequestBody enterprise: CreateGroupModel,
    ): String {
        throwUnauthorizedIfSo(token).also {
            return dbService.createGroup(removeBearerFromToken(token), enterprise)
        }
    }

    @GetMapping("/groups/{groupID}/enterprise")
    fun isEnterprise(
        @PathVariable("groupID") groupID: String
    ): String? {
        return dbService.isEnterprise(groupID = groupID)
    }


    @GetMapping("/groups/{groupID}")
    fun getAllGroupsImIn(
        @RequestHeader("Authorization") token: String,
        @PathVariable("groupID") groupID: String
    ): List<GroupNameId> {
        throwUnauthorizedIfSo(token).also {
            return dbService.getAllGroupsImIn(groupID = groupID, token = removeBearerFromToken(token))
        }
    }


    @PostMapping("/groups/{groupID}")
    fun joinGroup(
        @RequestHeader("Authorization") token: String,
        @PathVariable("groupID") groupID: String
    ) {
        throwUnauthorizedIfSo(token).also {
            dbService.joinGroup(groupID = groupID, token = removeBearerFromToken(token))
        }
    }

    @DeleteMapping("/groups/{groupID}")
    fun leaveGroup(
        @RequestHeader("Authorization") token: String,
        @PathVariable("groupID") groupID: String
    ) {
        throwUnauthorizedIfSo(token).also {
            dbService.leaveGroup(groupID = groupID, token = removeBearerFromToken(token))
        }
    }

    // TODO: get mapping to get all vehicles of group groups/groupid/vehicles...
    @GetMapping("/groups/{groupID}/vehicles")
    fun getAllGroupVehicles(
        @RequestHeader("Authorization") token: String,
        @PathVariable("groupID") groupID: String
    ): List<Vehicle> {
        throwUnauthorizedIfSo(token).also {
            return dbService.getAllGroupVehicles(groupID = groupID, token = removeBearerFromToken(token))
        }
    }

    @GetMapping("/groups/{groupID}/vehicles/{vehicleID}/refuels")
    fun getAllVehicleRefuelEntries(
        @RequestHeader("Authorization") token: String,
        @PathVariable("groupID") groupID: String,
        @PathVariable("vehicleID") vehicleID: String
    ): List<RefuelEntry> {
        throwUnauthorizedIfSo(token).also {
            return dbService.getAllVehicleRefuelEntries(
                groupID = groupID,
                token = removeBearerFromToken(token),
                vehicleID = vehicleID
            )
        }
    }

    @GetMapping("/groups/{groupID}/vehicles/{vehicleID}/routes")
    fun getAllRouteEntries(
        @RequestHeader("Authorization") token: String,
        @PathVariable("groupID") groupID: String,
        @PathVariable("vehicleID") vehicleID: String
    ): List<RouteEntry> {
        throwUnauthorizedIfSo(token).also {
            return dbService.getAllRouteEntries(
                groupID = groupID,
                token = removeBearerFromToken(token),
                vehicleID = vehicleID
            )
        }
    }

    private fun throwUnauthorizedIfSo(token: String) =
        authService.isAuthorized(removeBearerFromToken(token)).not().also {
            if (it) throw ResponseStatusException(
                HttpStatus.UNAUTHORIZED, "User with specified id not found/ does not exist"
            )
        }

    private fun removeBearerFromToken(token: String): String = token.replace("Bearer ", "")

}
