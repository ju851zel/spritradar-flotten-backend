FROM maven:3.8.4-openjdk-17 AS builder
COPY . /app
WORKDIR /app
RUN mvn clean package

FROM openjdk:17.0.1
WORKDIR /usr/local
EXPOSE 8081
COPY --from=builder /app/target/spritradar-flotten-0.0.1-SNAPSHOT.jar ./spritradar-server.jar
CMD [ "java", "-jar", "/usr/local/spritradar-server.jar" ]
